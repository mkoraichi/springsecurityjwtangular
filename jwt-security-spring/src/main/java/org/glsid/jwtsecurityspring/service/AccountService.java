package org.glsid.jwtsecurityspring.service;

import org.glsid.jwtsecurityspring.entities.AppRole;
import org.glsid.jwtsecurityspring.entities.AppUser;
import org.glsid.jwtsecurityspring.entities.Task;

import java.util.List;

public interface AccountService {
    Task saveTask(Task t);
    List<Task> getAllTasks();
    AppUser saveUser(AppUser user);
    List<AppUser> getAllUsers();
    AppRole saveRole(AppRole role);
    List<AppRole> getAllRoles();
    void addRoleToUser(String username,String role);
    AppUser findUser(String username);
    AppRole findRole(String role);
}
