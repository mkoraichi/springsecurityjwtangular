package org.glsid.jwtsecurityspring.security;

public class SecurityConstants {
    public static final String SECRET="msbkoraichi@gmail.com";
    public static final String HEADER="Authorization";
    public static final String TOKEN_PREFIX="Bearer";
    public static final long EXPIRATION_TIME=846_000_000;

}
