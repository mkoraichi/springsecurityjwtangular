package org.glsid.jwtsecurityspring.dao;

import org.glsid.jwtsecurityspring.entities.AppUser;
import org.glsid.jwtsecurityspring.entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppUserRepository extends JpaRepository<AppUser,Long> {
    AppUser findByUsername(String username);
}
