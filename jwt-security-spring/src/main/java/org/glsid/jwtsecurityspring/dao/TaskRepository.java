package org.glsid.jwtsecurityspring.dao;

import org.glsid.jwtsecurityspring.entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task,Long> {

}
