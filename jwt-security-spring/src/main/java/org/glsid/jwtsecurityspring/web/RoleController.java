package org.glsid.jwtsecurityspring.web;

import org.glsid.jwtsecurityspring.entities.AppRole;
import org.glsid.jwtsecurityspring.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RoleController implements AppController<AppRole> {
    @Autowired
    AccountService accountService;

    @GetMapping("/roles")
    @Override
    public List<AppRole> getAll() {
        return accountService.getAllRoles();
    }

    @PostMapping("/role")
    @Override
    public AppRole save(@RequestBody AppRole role) {
        return accountService.saveRole(role);
    }

}
