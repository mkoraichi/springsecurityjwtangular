package org.glsid.jwtsecurityspring.web;

import org.glsid.jwtsecurityspring.entities.Task;
import org.glsid.jwtsecurityspring.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TaskController implements AppController<Task> {
    @Autowired
    AccountService accountService;

    @GetMapping("/tasks")
    @Override
    public List<Task> getAll() {
        return accountService.getAllTasks();
    }

    @PostMapping("/task")
    @Override
    public Task save(@RequestBody Task task) {
        return accountService.saveTask(task);
    }

}
