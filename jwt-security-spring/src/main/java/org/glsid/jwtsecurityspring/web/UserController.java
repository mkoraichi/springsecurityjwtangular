package org.glsid.jwtsecurityspring.web;

import org.glsid.jwtsecurityspring.entities.AppUser;
import org.glsid.jwtsecurityspring.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController implements AppController<AppUser> {
    @Autowired
    AccountService accountService;

    @GetMapping("/users")
    @Override
    public List<AppUser> getAll() {
        return accountService.getAllUsers();
    }

    @PostMapping("/user")
    @Override
    public AppUser save(@RequestBody AppUser user) {
        if(accountService.findUser(user.getUsername())!=null)
            throw new RuntimeException("this username is already exists");
        return accountService.saveUser(user);
    }

    /*@PostMapping("/user/privilege")
    public void givePrivilege(@RequestBody AppUser user) {
        accountService.addRoleToUser(user.getUsername(),user.getRoles().get(0).getRole());
    }*/
}
