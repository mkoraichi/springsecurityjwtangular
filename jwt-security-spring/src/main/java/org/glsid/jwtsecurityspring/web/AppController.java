package org.glsid.jwtsecurityspring.web;

import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface AppController<T> {
    public List<T> getAll();

    public T save(@RequestBody T t);
}
